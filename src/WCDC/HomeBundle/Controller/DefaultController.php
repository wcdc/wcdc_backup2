<?php

namespace WCDC\HomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('WCDCHomeBundle:Default:index.html.twig');
    }
}
