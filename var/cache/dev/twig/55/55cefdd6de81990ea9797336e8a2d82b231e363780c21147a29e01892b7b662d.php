<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_7361f66283bfd3f64d9d5ce59bc636d786e3d6fd2c6a599aec16ebc4098d35f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_91ef2f50151dfc49dd41378dcb8ed7a8d19a4fa7ed67b4559281a28d242f9de7 = $this->env->getExtension("native_profiler");
        $__internal_91ef2f50151dfc49dd41378dcb8ed7a8d19a4fa7ed67b4559281a28d242f9de7->enter($__internal_91ef2f50151dfc49dd41378dcb8ed7a8d19a4fa7ed67b4559281a28d242f9de7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_91ef2f50151dfc49dd41378dcb8ed7a8d19a4fa7ed67b4559281a28d242f9de7->leave($__internal_91ef2f50151dfc49dd41378dcb8ed7a8d19a4fa7ed67b4559281a28d242f9de7_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_b63a4d0bfd6b23bf7b270159ae37233ea0608c703a4ce675afb9936bff45bb28 = $this->env->getExtension("native_profiler");
        $__internal_b63a4d0bfd6b23bf7b270159ae37233ea0608c703a4ce675afb9936bff45bb28->enter($__internal_b63a4d0bfd6b23bf7b270159ae37233ea0608c703a4ce675afb9936bff45bb28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_b63a4d0bfd6b23bf7b270159ae37233ea0608c703a4ce675afb9936bff45bb28->leave($__internal_b63a4d0bfd6b23bf7b270159ae37233ea0608c703a4ce675afb9936bff45bb28_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_f21664095debb7673c38e040372db6143f5ed37c7265c9e6cc9a7fca31a16768 = $this->env->getExtension("native_profiler");
        $__internal_f21664095debb7673c38e040372db6143f5ed37c7265c9e6cc9a7fca31a16768->enter($__internal_f21664095debb7673c38e040372db6143f5ed37c7265c9e6cc9a7fca31a16768_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_f21664095debb7673c38e040372db6143f5ed37c7265c9e6cc9a7fca31a16768->leave($__internal_f21664095debb7673c38e040372db6143f5ed37c7265c9e6cc9a7fca31a16768_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_e43f88a559fcfae13506a63347c934e0431aaa2fd5183a43083935bbf3a092ff = $this->env->getExtension("native_profiler");
        $__internal_e43f88a559fcfae13506a63347c934e0431aaa2fd5183a43083935bbf3a092ff->enter($__internal_e43f88a559fcfae13506a63347c934e0431aaa2fd5183a43083935bbf3a092ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_e43f88a559fcfae13506a63347c934e0431aaa2fd5183a43083935bbf3a092ff->leave($__internal_e43f88a559fcfae13506a63347c934e0431aaa2fd5183a43083935bbf3a092ff_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
