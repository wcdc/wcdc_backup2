<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_2ca72fdeb50ae05f5ad2830e821aef47507318c0cf88d7bdb92e8ac73b6abdb9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e96e427bba46e937e099ae53fa24a43d75969810766e77ab20091b2cc381f022 = $this->env->getExtension("native_profiler");
        $__internal_e96e427bba46e937e099ae53fa24a43d75969810766e77ab20091b2cc381f022->enter($__internal_e96e427bba46e937e099ae53fa24a43d75969810766e77ab20091b2cc381f022_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e96e427bba46e937e099ae53fa24a43d75969810766e77ab20091b2cc381f022->leave($__internal_e96e427bba46e937e099ae53fa24a43d75969810766e77ab20091b2cc381f022_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_af36eef2f4f0349f27fe37d6355b261765228fde35ed5ff37549c09d7eb71f9a = $this->env->getExtension("native_profiler");
        $__internal_af36eef2f4f0349f27fe37d6355b261765228fde35ed5ff37549c09d7eb71f9a->enter($__internal_af36eef2f4f0349f27fe37d6355b261765228fde35ed5ff37549c09d7eb71f9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_af36eef2f4f0349f27fe37d6355b261765228fde35ed5ff37549c09d7eb71f9a->leave($__internal_af36eef2f4f0349f27fe37d6355b261765228fde35ed5ff37549c09d7eb71f9a_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_a6a616a3748d597147e2626989b58a516fd9717aab4d9de8fe2bf0d8421cc10e = $this->env->getExtension("native_profiler");
        $__internal_a6a616a3748d597147e2626989b58a516fd9717aab4d9de8fe2bf0d8421cc10e->enter($__internal_a6a616a3748d597147e2626989b58a516fd9717aab4d9de8fe2bf0d8421cc10e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_a6a616a3748d597147e2626989b58a516fd9717aab4d9de8fe2bf0d8421cc10e->leave($__internal_a6a616a3748d597147e2626989b58a516fd9717aab4d9de8fe2bf0d8421cc10e_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_cd05742e85a6d4973fac757dfe8675f3bfd4e29ed8fe0e1ec53190754968801f = $this->env->getExtension("native_profiler");
        $__internal_cd05742e85a6d4973fac757dfe8675f3bfd4e29ed8fe0e1ec53190754968801f->enter($__internal_cd05742e85a6d4973fac757dfe8675f3bfd4e29ed8fe0e1ec53190754968801f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_cd05742e85a6d4973fac757dfe8675f3bfd4e29ed8fe0e1ec53190754968801f->leave($__internal_cd05742e85a6d4973fac757dfe8675f3bfd4e29ed8fe0e1ec53190754968801f_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
